﻿using Castle.Core.Smtp;
using CustomerCommLib;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerComm.Tests
{
    [TestFixture]
    public class CustomerCommTest
    {
        [Test]
        
       [OneTimeSetUp]
        public void MailSenderTest()
        {
            CustomerCommLib.MailSender mailSender = new CustomerCommLib.MailSender();
            var SenderMailMock = new Mock<IMailSender>();
            CustomerCommLib.CustomerComm customerComm = new CustomerCommLib.CustomerComm(SenderMailMock.Object);
            bool send = customerComm.SendMailToCustomer();
          
            Assert.That(true, Is.EqualTo(send));
        }
    }
}
