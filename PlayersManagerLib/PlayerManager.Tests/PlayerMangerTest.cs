﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using PlayersManagerLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assert = NUnit.Framework.Assert;

namespace PlayerManager.Tests
{
    [TestFixture]
    public class PlayerMangerTest
    {
        [Test]
        [OneTimeSetUp]
        [ExpectedException(typeof(Exception))]
        public void PlayerMangerTests()
        {
            PlayersManagerLib.PlayerMapper playerMapper = new PlayersManagerLib.PlayerMapper();
            var MockPlayerManger = new Mock<IPlayerMapper>();
            PlayersManagerLib.Player player = new PlayersManagerLib.Player("Akshith", 23, "India", 30);
            PlayersManagerLib.Player player1 = PlayersManagerLib.Player.RegisterNewPlayer("Akshith", MockPlayerManger.Object);
            bool res = playerMapper.IsPlayerNameExistsInDb("Akshith");
            Assert.That(false, Is.EqualTo(res));

        }

       
    }
}
