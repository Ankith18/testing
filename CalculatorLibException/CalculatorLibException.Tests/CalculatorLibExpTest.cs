﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assert = NUnit.Framework.Assert;

namespace CalculatorLibException.Tests
{
    [TestFixture]
    public class CalculatorLibExpTest
    {
       
        [Test]

        public void DivideTestConstraint()
        {
            CalculatorLibException.CalExp calExp = new CalculatorLibException.CalExp();

            var ex = Assert.Throws<DivideByZeroException>(() => calExp.Divide(10, 0));
            Assert.That("Attempted to divide by zero.", Is.EqualTo(ex.Message));
        }
        }
}
