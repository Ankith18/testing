﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestCalculatorException
{
    [TestClass]
    public class UnitTest1
    {
        
            [TestMethod]
            [ExpectedException(typeof(DivideByZeroException))]
            public void DivideTest()
            {
                CalculatorLibException.CalExp calExp = new CalculatorLibException.CalExp();
                int res = calExp.Divide(10, 0);

            }
        
    }
}
