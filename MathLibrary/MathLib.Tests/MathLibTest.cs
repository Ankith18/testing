﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLib.Tests
{
    
    [TestFixture]
    public class MathLibTest
    {
        private static IEnumerable<TestCaseData> CompareTwoNumbersTestData
        {
            get
            {
                yield return new TestCaseData(-1, 0, 0);
                yield return new TestCaseData(1, 2, -1);
                yield return new TestCaseData(2, 1, 1);
            }
        }
        [Test]
        [TestCaseSource(nameof(CompareTwoNumbersTestData))]
        public void CompareTwoNumbersTest(int firstNo, int secondNo,int expRes)
        {
            MathLibrary.MathLib mathLib = new MathLibrary.MathLib();
            int res = mathLib.CompareTwoNumbers(firstNo, secondNo);
            Assert.That(res, Is.EqualTo(expRes));

        }
       
    }
}
