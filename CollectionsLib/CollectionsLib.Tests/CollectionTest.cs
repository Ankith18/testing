﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsLib.Tests
{
    [TestFixture]
    public class CollectionTest
    {
        [Test]
        public void GetEmployeesNullTest()
        {
            CollectionsLib.EmployeeManager employeeManager = new CollectionsLib.EmployeeManager();
            List<Employee> employee = employeeManager.GetEmployees();
            Assert.That(employee, Is.Not.EqualTo(null));

        }
        [Test]
        public void GetEmployeesIdTest()
        {
            CollectionsLib.EmployeeManager employeeManager = new CollectionsLib.EmployeeManager();
            List<Employee> employee = employeeManager.GetEmployees();
            foreach(var x in employee) {
                Assert.That(x.EmpId, Is.EqualTo(100));
                break;
            }

        }
       
        CollectionsLib.Employee Employee = new CollectionsLib.Employee();
       
        [Test]
        public void GetEmployeesIdUnique()
        {
            CollectionsLib.EmployeeManager employeeManager = new CollectionsLib.EmployeeManager();
            
            List<Employee> employee = employeeManager.GetEmployees();
            foreach (var x in employee)
            {
                Assert.That(Employee.EmpId.GetHashCode, Is.Not.EqualTo(x.EmpId));
            }

        }
        [Test]
        public void ConstraintAssertionModel()
        {
            CollectionsLib.EmployeeManager employeeManager = new CollectionsLib.EmployeeManager();

            List<Employee> employee = employeeManager.GetEmployees();
            List<Employee> employees = employeeManager.GetEmployeesWhoJoinedInPreviousYears();
            
                Assert.That(employee, Is.EqualTo(employees));
            

        }
        [Test]
        public void ClassicAssertionModel()
        {
            CollectionsLib.EmployeeManager employeeManager = new CollectionsLib.EmployeeManager();

            List<Employee> employee = employeeManager.GetEmployees();
            List<Employee> employees = employeeManager.GetEmployeesWhoJoinedInPreviousYears();

            Assert.AreEqual(employee,(employees));


        }
    }
}
