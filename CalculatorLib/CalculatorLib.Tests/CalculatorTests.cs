﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorLib.Tests
{
    [TestFixture]
    public class CalculatorTests
    {
        [Test]
        public void UnitUnderTest_Scenario_ExpectedOutcome()
        {
            CalculatorLib.Calculator calculator = new CalculatorLib.Calculator();
            int res = calculator.Add(10, 20);
            Assert.AreEqual(30, 30);
        }
        [Test]
        public void UnitUnderTest_Scenario_ExpectedOutcome40()
        {
            CalculatorLib.Calculator calculator = new CalculatorLib.Calculator();
            int res = calculator.Add(20, 20);
            Assert.AreEqual(40, 40);
        }
        [Test]
        public void UnitUnderTest_Scenario_ExpectedOutcome0()
        {
            CalculatorLib.Calculator calculator = new CalculatorLib.Calculator();
            int res = calculator.Add(10, 0);
            Assert.AreEqual(0, 0);
        }
        [Test]
        public void UnitUnderTest_Scenario_ExpectedOutcome30()
        {
            CalculatorLib.Calculator calculator = new CalculatorLib.Calculator();
            int res = calculator.Add(10, 10);
            Assert.AreEqual(20, 30);
        }
    }
}
