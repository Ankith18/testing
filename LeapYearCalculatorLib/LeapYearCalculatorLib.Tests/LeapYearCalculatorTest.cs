﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeapYearCalculatorLib.Tests
{
    [TestFixture]
    public class LeapYearCalculatorTest
    {
        [Test]
        public void IsLeapYearTest2020()
        {
            LeapYearCalculatorLib.LeapYearCalculator leapYearCalculator = new LeapYearCalculatorLib.LeapYearCalculator();
            int res = leapYearCalculator.IsLeapYear(2020);
            Assert.That(res, Is.EqualTo(1));
        }
        [Test]
        public void IsLeapYearTest2019()
        {
            LeapYearCalculatorLib.LeapYearCalculator leapYearCalculator = new LeapYearCalculatorLib.LeapYearCalculator();
            int res = leapYearCalculator.IsLeapYear(2029);
            Assert.That(res, Is.EqualTo(0));
        }
        [Test]
        public void IsLeapYearTest1750()
        {
            LeapYearCalculatorLib.LeapYearCalculator leapYearCalculator = new LeapYearCalculatorLib.LeapYearCalculator();
            int res = leapYearCalculator.IsLeapYear(1750);
            Assert.That(res, Is.EqualTo(-1));
        }
    }
}
