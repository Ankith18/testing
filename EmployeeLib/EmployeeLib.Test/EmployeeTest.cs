﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeLib.Test
{
    [TestFixture]
    public class EmployeeTest
    {
        [Test]
        public void AddEmployeeTest0()
        {
            EmployeeLib.EmployeeManger employee = new EmployeeLib.EmployeeManger();
            Employee emp = new Employee();

            Assert.That(0, Is.EqualTo(employee.HeadCount));
        }
        [Test]
        public void AddEmployeeTest1()
        {
            EmployeeLib.EmployeeManger employee = new EmployeeLib.EmployeeManger();
            Employee emp = new Employee();
            emp.Name = "Anush";
            emp.Id = 1;
            employee.AddEmployees(emp);
            Assert.That(1, Is.EqualTo(employee.HeadCount));
        }
    }
}
