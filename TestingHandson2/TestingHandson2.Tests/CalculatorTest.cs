﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingHandson2.Tests
{
    [TestFixture]
    public class CalculatorTest
    {
        [Test, Order(1)]
        [TestCaseSource(nameof(AddCases))]
        [Author("Developer", "arun@gmail.com")]
        [Category("Math")]
        public void AddTest(int firstNo,int secondNo)
        {
            TestingHandson2.Calculator calculator = new TestingHandson2.Calculator();
            int sum = calculator.Add(firstNo, secondNo);
            Assert.That(sum, Is.EqualTo(firstNo + secondNo));
        }
        static readonly object[] AddCases =
        {
            new object[]{1,1},
            new object[]{50,50},
            new object[]{22,13}


        };
        [Test, Order(2)]
        [TestCaseSource(nameof(MulCases))]
        [Author("Developer", "arun@gmail.com")]
        [Category("Math")]
        public void MulTest(int firstNo, int secondNo)
        {
            TestingHandson2.Calculator calculator = new TestingHandson2.Calculator();
            int sum = calculator.Mul(firstNo, secondNo);
            Assert.That(sum, Is.EqualTo(firstNo * secondNo));
        }
        static readonly object[] MulCases =
        {
            new object[]{1,1},
            new object[]{50,50},
            new object[]{22,13}


        };
        [Test, Order(3)]
        [TestCaseSource(nameof(SubCases))]
        [Author("Developer", "arun@gmail.com")]
        [Category("Math")]
        public void SubTest(int firstNo, int secondNo)
        {
            TestingHandson2.Calculator calculator = new TestingHandson2.Calculator();
            int sum = calculator.Sub(firstNo, secondNo);
            Assert.That(sum, Is.EqualTo(firstNo - secondNo));
        }
        static readonly object[] SubCases =
        {
            new object[]{1,1},
            new object[]{50,50},
            new object[]{22,13}


        };
        [Test, Order(4)]
        [TestCaseSource(nameof(DivCases))]
        [Author("Developer", "arun@gmail.com")]
        [Category("Math")]
        [Ignore("Ignore a test")]
        public void DivTest(int firstNo, int secondNo)
        {
            TestingHandson2.Calculator calculator = new TestingHandson2.Calculator();
            int sum = calculator.Mul(firstNo, secondNo);
            Assert.That(sum, Is.EqualTo(firstNo / secondNo));
        }
        static readonly object[] DivCases =
        {
            new object[]{1,1},
            new object[]{50,50},
            new object[]{22,13}


        };
    }
}
