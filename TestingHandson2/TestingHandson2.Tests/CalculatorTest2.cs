﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingHandson2.Tests
{
    [TestFixture]
   public class CalculatorTest2
    {

        [Test, Order(1)]
        [TestCaseSource(typeof(AddCases))]
        [Author("Developer", "arun@gmail.com")]
        [Category("Math")]
        public void AddTest(int firstNo, int secondNo)
            {
                TestingHandson2.Calculator calculator = new TestingHandson2.Calculator();
                int sum = calculator.Add(firstNo, secondNo);
                Assert.That(sum, Is.EqualTo(firstNo + secondNo));
            }
       
        [Test, Order(2)]
        [TestCaseSource(typeof(MulCases))]
        [Author("Developer", "arun@gmail.com")]
        [Category("Math")]
        public void MulTest(int firstNo, int secondNo)
        {
            TestingHandson2.Calculator calculator = new TestingHandson2.Calculator();
            int sum = calculator.Mul(firstNo, secondNo);
            Assert.That(sum, Is.EqualTo(firstNo *secondNo));
        }
        class MulCases : IEnumerable
        {
            public IEnumerator GetEnumerator()
            {
                yield return new object[] { 1, 1 };
                yield return new object[] { 50, 50 };
                yield return new object[] { 22, 13 };
            }
        }
        [Test, Order(3)]
        [TestCaseSource(typeof(SubCases))]
        [Author("Developer", "arun@gmail.com")]
        [Category("Math")]
        public void SubTest(int firstNo, int secondNo)
        {
            TestingHandson2.Calculator calculator = new TestingHandson2.Calculator();
            int sum = calculator.Sub(firstNo, secondNo);
            Assert.That(sum, Is.EqualTo(firstNo - secondNo));
        }
       
        [Test, Order(4)]
        [TestCaseSource(typeof(DivCases))]
        [Author("Developer", "arun@gmail.com")]
        [Category("Math")]
        [Ignore("Ignore a test")]
        public void DivTest(int firstNo, int secondNo)
        {
            TestingHandson2.Calculator calculator = new TestingHandson2.Calculator();
            int sum = calculator.Mul(firstNo, secondNo);
            Assert.That(sum, Is.EqualTo(firstNo / secondNo));
        }
        
    }
    class AddCases : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            yield return new object[] { 1, 1 };
            yield return new object[] { 50, 50 };
            yield return new object[] { 22, 13 };
        }
    }
    class DivCases : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            yield return new object[] { 1, 1 };
            yield return new object[] { 50, 50 };
            yield return new object[] { 22, 13 };
        }
    }
    class SubCases : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            yield return new object[] { 1, 1 };
            yield return new object[] { 50, 50 };
            yield return new object[] { 22, 13 };
        }
    }
}
